package com.netcracker.bulis.service;

import com.netcracker.bulis.dao.DefaultPersonDao;
import com.netcracker.bulis.dao.PersonDao;
import com.netcracker.bulis.dto.Employer;
import com.netcracker.bulis.entity.Person;
import com.netcracker.bulis.exception.NotPersonException;
import com.netcracker.bulis.exception.PersonNotFoundException;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class DefaultPersonService implements PersonService {
    private PersonDao personDao = new DefaultPersonDao();

    @Override
    public UUID save(Employer employer) {
        Person person = createPerson(employer);
        String id = personDao.savePerson(person);
        return UUID.fromString(id);
    }

    private Person createPerson(Employer employer) {
        return new Person(employer.getName(), (byte) employer.getAge());
    }

    @Override
    public Person get(UUID uuid) {
        Person person = null;
        try {
            person = personDao.getPerson(uuid.toString());
        } catch (PersonNotFoundException | NotPersonException e) {
            System.err.println("ERROR");
        }
        return person;
    }

    @Override
    public void saveAll(List<Person> persons) {
        for (Person p : persons) {
            personDao.savePerson(p);
        }
    }

    @Override
    public List<Person> findAll() {
        return personDao.getAllPersons();
    }

    @Override
    public List<Person> findByName(String name) {
        List<Person> allPersons = personDao.getAllPersons();

        return allPersons.stream()
                .filter(person -> person.getName().equals(name))
                .collect(Collectors.toList());
    }

    @Override
    public List<Person> findByAge(byte age) {
        List<Person> allPersons = personDao.getAllPersons();

        return allPersons.stream()
                .filter(person -> person.getAge() == age)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteById(String id) throws NotPersonException, PersonNotFoundException {
        personDao.deletePerson(id);
    }

    @Override
    public void deleteByName(String name) throws NotPersonException, PersonNotFoundException {
        List<Person> allPersonsWithTheName = personDao.getAllPersons().stream()
                .filter(person -> person.getName().equals(name))
                .collect(Collectors.toList());
        for (Person person : allPersonsWithTheName) {
            personDao.deletePerson(person.getId());
        }
    }

    @Override
    public void deleteAll() throws NotPersonException, PersonNotFoundException {
        List<Person> allPersons = personDao.getAllPersons();
        for (Person person : allPersons) {
            personDao.deletePerson(person.getId());
        }
    }

    @Override
    public List<Person> findByNameAndAge(String name, byte age) {
        List<Person> allPersonsWithNameAndAge = personDao.getAllPersons();

        return allPersonsWithNameAndAge.stream()
                .filter(person -> person.getName().equals(name) && person.getAge() == age)
                .collect(Collectors.toList());
    }
}

