package com.netcracker.bulis.service;

import com.netcracker.bulis.dto.Employer;
import com.netcracker.bulis.entity.Person;
import com.netcracker.bulis.exception.NotPersonException;
import com.netcracker.bulis.exception.PersonNotFoundException;

import java.util.List;
import java.util.UUID;

public interface PersonService {
    UUID save(Employer employer);
    Person get(UUID uuid);
    void saveAll(List <Person> persons);
    List<Person> findAll();
    List<Person> findByName(String name);
    List<Person> findByAge (byte age);
    void deleteById(String id) throws NotPersonException, PersonNotFoundException;
    void deleteByName(String name) throws NotPersonException, PersonNotFoundException;
    void deleteAll() throws NotPersonException, PersonNotFoundException;
    List<Person> findByNameAndAge(String name, byte age);
}
