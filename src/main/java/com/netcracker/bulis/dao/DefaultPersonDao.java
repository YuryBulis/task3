package com.netcracker.bulis.dao;

import com.netcracker.bulis.entity.Person;
import com.netcracker.bulis.exception.NotPersonException;
import com.netcracker.bulis.exception.PersonNotFoundException;
import com.netcracker.bulis.storage.TemporaryStorage;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DefaultPersonDao implements PersonDao {

    private TemporaryStorage storage = TemporaryStorage.getInstance();

    public DefaultPersonDao() {
    }

    public String savePerson(Person person) {
        String id = UUID.randomUUID().toString();
        person.setId(id);
        storage.insertEntity(id, person);
        return id;
    }

    public Person getPerson(String id) throws PersonNotFoundException, NotPersonException {
        Object entity = storage.getEntity(id);
        if (entity == null) {
            throw new PersonNotFoundException();
        }
        if (!Person.class.equals(entity.getClass())) {
            throw new NotPersonException();
        }
        return (Person) entity;
    }

    @Override
    public List<Person> getAllPersons() {
        List<Person> persons = new ArrayList<>();
        List<Object> objects = storage.getEntities();
        for (Object object : objects) {
            if (object.getClass().equals(Person.class)) {
                persons.add((Person) object);
            }
        }
        return persons;
    }

    @Override
    public void deletePerson(String id) throws PersonNotFoundException, NotPersonException{
        Object entity = storage.getEntity(id);
        if (entity == null) {
            throw new PersonNotFoundException();
        }
        if (!Person.class.equals(entity.getClass())) {
            throw new NotPersonException();
        }

        storage.deleteEntity(id);
    }



}
