package com.netcracker.bulis.dao;

import com.netcracker.bulis.entity.Person;
import com.netcracker.bulis.exception.NotPersonException;
import com.netcracker.bulis.exception.PersonNotFoundException;

import java.util.List;

public interface PersonDao {
    String savePerson (Person person);
    Person getPerson(String id) throws PersonNotFoundException, NotPersonException;
    List<Person> getAllPersons ();
    void deletePerson(String id) throws PersonNotFoundException, NotPersonException;
}
