package com.netcracker.bulis;

import com.netcracker.bulis.dao.DefaultPersonDao;
import com.netcracker.bulis.entity.Person;
import com.netcracker.bulis.exception.NotPersonException;
import com.netcracker.bulis.exception.PersonNotFoundException;
import com.netcracker.bulis.service.DefaultPersonService;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Person person = new Person("Alex", (byte) 26);
        Person person1 = new Person("Tom", (byte) 18);
        Person person2 = new Person("Mike", (byte) 34);
        Person person3 = new Person("Alex", (byte) 41);
        DefaultPersonDao defaultPersonDao = new DefaultPersonDao();
        DefaultPersonService defaultPersonService = new DefaultPersonService();

        List <Person> personList = new ArrayList<>();
        personList.add(person);
        personList.add(person1);
        personList.add(person2);
        personList.add(person3);
        defaultPersonService.saveAll(personList);


        System.out.println(defaultPersonService.findByName("Alex") + "-----");




        String id = defaultPersonDao.savePerson(person);

        try{
            Person getPerson = defaultPersonDao.getPerson(id);
            System.out.println(getPerson);
        }catch(PersonNotFoundException | NotPersonException e){
            e.printStackTrace();
        }

        try{
            defaultPersonDao.getPerson(id);
        } catch (PersonNotFoundException | NotPersonException e) {
            e.printStackTrace();
        }

    }
}
