import com.netcracker.bulis.dao.DefaultPersonDao;
import com.netcracker.bulis.dao.PersonDao;
import com.netcracker.bulis.dto.Employer;
import com.netcracker.bulis.entity.Person;
import com.netcracker.bulis.exception.NotPersonException;
import com.netcracker.bulis.exception.PersonNotFoundException;
import com.netcracker.bulis.service.DefaultPersonService;
import com.netcracker.bulis.service.PersonService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class Test {

    @InjectMocks
    private PersonService personService = new DefaultPersonService();

    private DefaultPersonService defaultPersonService = new DefaultPersonService();
    @Mock
    private PersonDao personDao;

    private PersonDao personDao2 = new DefaultPersonDao();

    private Employer employer;
    private Person person;
    private String id;
    private UUID uuid;
    private Person person2 = new Person("Alex", (byte) 26);
    private Person person3 = new Person("Tom", (byte) 18);
    private Person person4 = new Person("Mike", (byte) 34);
    private Person person5 = new Person("Alex", (byte) 41);

    private List<Person> personList = new ArrayList<>();

    @BeforeEach
    public void init() {

        employer = new Employer();
        employer.setAge(30);
        employer.setName("Dima");
        employer.setSurname("Pushkin");

        uuid = UUID.randomUUID();
        id = UUID.randomUUID().toString();

        person = new Person("Alex", (byte) 25);
    }

    @org.junit.jupiter.api.Test
    public void save() {
        when(personDao.savePerson(any(Person.class))).thenReturn(id);

        UUID uuid = personService.save(employer);
        assertNotNull(uuid);
    }

    @org.junit.jupiter.api.Test
    public void get() throws NotPersonException, PersonNotFoundException {
        when(personDao.getPerson(uuid.toString())).thenReturn(person);
        Person person = personService.get(uuid);

        assertNotNull(person);
    }

    @org.junit.jupiter.api.Test
    public void findByName() {
        personList.add(person2);
        personList.add(person3);
        personList.add(person4);
        personList.add(person5);
        defaultPersonService.saveAll(personList);
        assertEquals(person3, defaultPersonService.findByName("Tom").get(0));
        try {
            defaultPersonService.deleteAll();
        } catch (NotPersonException | PersonNotFoundException e) {
            e.printStackTrace();
        }
    }

    @org.junit.jupiter.api.Test
    public void findByAge() {
        personList.add(person5);
        personList.add(person4);
        personList.add(person3);
        personList.add(person2);
        defaultPersonService.saveAll(personList);
        assertEquals(person4, defaultPersonService.findByAge((byte) 34).get(0));
        try {
            defaultPersonService.deleteAll();
        } catch (NotPersonException | PersonNotFoundException e) {
            e.printStackTrace();
        }
    }

    @org.junit.jupiter.api.Test
    public void deleteAll() {
        personList.add(person5);
        personList.add(person4);
        personList.add(person3);
        personList.add(person2);
        defaultPersonService.saveAll(personList);
        try {
            defaultPersonService.deleteAll();
        } catch (NotPersonException | PersonNotFoundException e) {
            e.printStackTrace();
        }
        assertEquals(new ArrayList<>(), defaultPersonService.findAll());
    }

    @org.junit.jupiter.api.Test
    public void deleteByName() throws NotPersonException, PersonNotFoundException {
        personList.add(person5);
        personList.add(person4);
        personList.add(person3);
        personList.add(person2);
        defaultPersonService.saveAll(personList);
        defaultPersonService.deleteByName("Tom");

        assertEquals(new ArrayList<>(), defaultPersonService.findByName("Tom"));
        defaultPersonService.deleteAll();
    }

    @org.junit.jupiter.api.Test
    public void deleteById() throws NotPersonException, PersonNotFoundException {
        personList.add(person5);
        personList.add(person4);
        personList.add(person3);
        personList.add(person2);
        defaultPersonService.saveAll(personList);

        defaultPersonService.deleteById(person3.getId());
        assertEquals(new ArrayList<Person>(), defaultPersonService.findByName(person3.getName()));
        defaultPersonService.deleteAll();
    }

    @org.junit.jupiter.api.Test
    public void personNotFoundException() {
        boolean exception = false;
        try {
            personDao2.getPerson("ds");
        } catch (PersonNotFoundException e) {
            exception = true;
        } catch (NotPersonException e) {
            e.printStackTrace();
        }
        assertTrue(exception);
    }


}
